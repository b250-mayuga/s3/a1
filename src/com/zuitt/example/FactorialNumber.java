package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args){
        System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);


        int answer = 1;

        try{
            int num = in.nextInt();

                if(num < 0){
                    System.out.println("Please enter a positive integer.");
                }
                else {
                    for(int i = 1; i <= num; i++ )
                    answer = answer * i;

                    System.out.println("The factorial of " + num + " is " + answer);
                }

        }
        catch(Exception e) {
            System.out.println("Invalid input. Please enter a number.");
            e.printStackTrace();
        }


    }
}
